
	<footer> <!-- Start of footer -->

        <div class="row footer-content">

            <div class="col-md-3 col-md-offset-3">

                <ul class="menu">
                    <a href="#">COMO FUNCIONA</a>
                    <a href="#">VANTAGENS</a>
                    <a href="#">PLANOS</a>
                    <a href="#">SALÕES</a>
                    <a href="#">BLOG</a>
                    <a href="#">SOBRE</a>
                    <a href="#">CONTATO</a>
                    <a href="#">FAQ</a>
                </ul>

            </div>

            <div class="col-md-3">

                <p class="address">
                    <strong>ENTRE EM CONTATO</strong><br />
                    Rua Lorem, 240 - Lorem Ipsum - PR <br />
                    (00) 0 0000-0000 <br />
                    (00) 0000-0000 <br />
                    atendimento@happy.com.br
                </p>

                <p>© Happy 2017</p>
                <p>Desenvolvido por DevMaker Apps</p>

            </div>

        </div>

        <script src="<?php echo $dir_fixer; ?>assets/js/jquery.js"></script>
        <script src="<?php echo $dir_fixer; ?>assets/js/bootstrap.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/select2/select2.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js"></script>
        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.pt.min.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/slimScroll/jquery.slimscroll.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/DataTables/media/js/datatables.js"></script>
        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/DataTables/media/js/dataTables.colReorder.js"></script>
        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/DataTables/media/js/dataTables.responsive.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/Morris/raphael.js"></script>
        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/Morris/morris.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/fullcalendar/moment.min.js"></script>
        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/fullcalendar/fullcalendar.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/slick/slick.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/plugins/dropify/dropify.js"></script>

        <script src="<?php echo $dir_fixer; ?>assets/js/script.js"></script>
	
	</footer> <!-- End of footer -->

</body>

</html>