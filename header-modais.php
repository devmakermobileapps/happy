
<div class="modal type-01" id="cadastre_se-modal" role="dialog">

    <div class="modal-dialog modal-md">

        <div class="modal-content">

            <div class="modal-body">

                <h3>Cadastre-se</h3>

                <p>Informe qual o seu tipo de cadastro</p>

                <div class="form-group">

                    <select class="select2 form-control type-03 lg" id="cadastro-choice">

                        <option value="<?php echo $dir_fixer; ?>internas/cadastro-pessoa-fisica.php">Convênio</option>
                        <option value="<?php echo $dir_fixer; ?>internas/cadastro-pessoa-juridica.php">Salão</option>
<!--                        <option value="--><?php //echo $dir_fixer; ?><!--internas/cadastro-pessoa-vendedor.php">Vendedor</option>-->

                    </select>

                </div>

            </div>

            <div class="modal-footer no-padding flex">

                <button class="btn third full" class="close" data-dismiss="modal">Cancelar</button>
                <button class="btn default full" id="btn-cadastro-choice">Enviar</button>

            </div>

        </div>

    </div>

</div>

<div class="modal type-01" id="recuperar_senha-modal" role="dialog">

    <div class="modal-dialog modal-md">

        <div class="modal-content">

            <div class="modal-body">

                <h3>Recuperar senha</h3>

                <p>Informe seu e-mail para receber o link para redefiniçāo de senha.</p>

                <div class="form-group">

                    <input type="text" class="form-control type-03 lg" placeholder="E-mail" />

                </div>

            </div>

            <div class="modal-footer no-padding flex">

                <button class="btn third full" class="close" data-dismiss="modal">Cancelar</button>
                <button class="btn default full">Enviar</button>

            </div>

        </div>

    </div>

</div>

<div class="modal type-01" id="login-modal" role="dialog">

    <div class="modal-dialog modal-md">

        <div class="modal-content">

            <div class="modal-body">

                <h3>Informe sua senha</h3>

                <div class="form-group">

                    <input type="text" class="form-control type-03 lg" placeholder="Senha" />

                </div>

                <p>Ou faça seu login com:</p>

                <button class="btn btn-facebook btn-block"><i class="fa fa-facebook-official"></i> ENTRAR COM FACEBOOK</button>

                <p class="forgot-password" data-toggle="modal" data-target="#recuperar_senha-modal">Esqueceu a senha?</p>

            </div>

            <div class="modal-footer no-padding flex">

                <button class="btn third full" class="close" data-dismiss="modal">Cancelar</button>
                <button class="btn default full">Enviar</button>

            </div>

        </div>

    </div>

</div>