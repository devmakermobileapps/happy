<!doctype html>

<html>

<?php include_once( 'init.php' ); ?>

<head>
	<title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<?php echo $dir_fixer; ?>assets/css/main.css" type="text/css" />
<!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( 'header-modais.php' ); ?>

    <div class="custom-navbar">

        <div class="container">

            <ul class="menu">

                <a href="<?php echo $dir_fixer; ?>index.php" class="menu-item">

                    <img src="<?php echo $dir_fixer; ?>assets/img/logo.png" alt="logo" />

                </a>

                <a href="<?php echo $dir_fixer; ?>internas/como-funciona.php" class="menu-item">COMO FUNCIONA</a>

                <a href="<?php echo $dir_fixer; ?>index.php" class="menu-item">VANTAGENS</a>
                <a href="<?php echo $dir_fixer; ?>internas/assine.php" class="menu-item">ASSINE</a>

                <li class="menu-item">

                    <a href="<?php echo $dir_fixer; ?>internas/saloes-conveniados.php">SALÕES CONVENIADOS</a>

                    <ul class="submenu">

                        <a href="<?php echo $dir_fixer; ?>internas/trabalhos-dos-profissionais.php" class="submenu-item">TRABALHOS DOS PROFISSIONAIS</a>

                    </ul>

                </li>

                <a href="<?php echo $dir_fixer; ?>internas/time-line.php" class="menu-item">TIME LINE</a>
                <a href="<?php echo $dir_fixer; ?>internas/sobre.php" class="menu-item">SOBRE</a>
                <a href="<?php echo $dir_fixer; ?>internas/contato.php" class="menu-item">CONTATO</a>

                <li href="#" class="menu-item">

                    <div class="form-group flex-row">

                        <input type="text" class="form-control type-01" placeholder="CPF ou E-mail" />

                        <button class="btn btn-default" data-toggle="modal" data-target="#login-modal">Entrar</button>

                    </div>

                    <div class="form-group">
                        <small class="cadastre-se" data-toggle="modal" data-target="#cadastre_se-modal">CADASTRE-SE GRÁTIS!</small>
                    </div>

                </li>

            </ul>

        </div>

    </div>