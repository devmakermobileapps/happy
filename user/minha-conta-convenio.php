<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo $dir_fixer; ?>assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../navbar-logado-convenio.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/minha-conta.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp
                </h2>

                <h1>CADASTRE-SE</h1>

            </div>

        </div>

    </div>

</div>

<!-- Start of Content -->

<section class="section">

    <div class="row minha-conta-convenio">

        <div class="col-md-6 lojas p-v-40 form-lg">

            <div class="form-group row">

                <div class="col-md-5">

                    <div class="image">

                        <img src="../assets/img/andreia.png" alt="andreia" class="img-responsive" />

                    </div>

                </div>

                <div class="col-md-7 account-info">

                    <table>

                        <tr>
                            <th>STATUS</th>
                            <th>VENCIMENTO DA ASSINATURA</th>
                        </tr>

                        <tr>
                            <td>ATIVO</td>
                            <td>01/01/2020</td>
                        </tr>

                    </table>

                    <ul class="menu">

                        <a href="<?php echo $dir_fixer; ?>user/meus-pedidos.php">Meus pedidos</a>
                        <a href="<?php echo $dir_fixer; ?>user/carrinho.php">Ver carrinho</a>
                        <a href="<?php echo $dir_fixer; ?>user/meus-pedidos.php">Histórico de compras</a>
                        <a href="#">2ª via do boleto</a>
                        <a href="#">Consultar cartão</a>

                    </ul>

                </div>

            </div>

            <div class="form-group">

                <label>Nome completo</label>
                <input type="text" class="form-control type-03 lg" />

            </div>

            <div class="form-group">

                <label>CPF</label>
                <input type="text" class="form-control type-03 lg" placeholder="000.000.000-00" />

            </div>

            <div class="form-group row">

                <div class="col-md-6">

                    <label>Estado</label>

                    <select class="select2 form-control type-03 lg">

                        <option>Escolher</option>

                    </select>

                </div>

                <div class="col-md-6">

                    <label>Cidade</label>

                    <select class="select2 form-control type-03 lg">

                        <option>Escolher</option>

                    </select>

                </div>

            </div>

            <div class="form-group row">

                <div class="col-md-6">

                    <label>Telefone</label>
                    <input type="text" class="form-control type-03 lg" placeholder="(00) 00000-0000" />

                </div>

                <div class="col-md-6">

                    <label>E-mail</label>
                    <input type="text" class="form-control type-03 lg" placeholder="email@seuemail.com.br" />

                </div>

            </div>

            <div class="form-group row">

                <div class="col-md-6">

                    <label>Gênero</label>

                    <select class="select2 form-control type-03 lg">

                        <option>Masculino</option>
                        <option>Feminino</option>

                    </select>

                </div>

                <div class="col-md-6">

                    <label>Data de nascimento</label>
                    <input type="text" class="form-control type-03 lg" />

                </div>

            </div>

            <div class="form-group">

                <button class="btn default btn-lg btn-block">ALTERAR SENHA</button>

            </div>

            <h5>ESCOLHA SEU PLANO</h5>

            <div class="form-group">

                <div class="input-group type-03 lg m-v-10">

                            <span class="input-group-addon">

                                <input type="radio" name="filter-1" id="filter-1" class="radio-default" />
                                <label for="filter-1"></label>

                            </span>

                    <p class="form-control-static">Serviços de cabelo/sobrancelhas/manicure.</p>

                </div>

            </div>

            <div class="form-group">

                <div class="input-group type-03 lg m-v-10">

                            <span class="input-group-addon">

                                <input type="radio" name="filter-1" id="filter-1" class="radio-default" />
                                <label for="filter-1"></label>

                            </span>

                    <p class="form-control-static">
                        PLANO OURO PLUS - R$29,90 - Serviços de cabelo/sobrancelha/manicure/micro pigmentação/estética/podólogo, dentre outros.
                    </p>

                </div>

            </div>

            <div class="form-group">

                <button  class="btn default btn-lg btn-block">ATUALIZAR DADOS</button>

            </div>

        </div>

        <div class="col-md-6">

            <h5 class="convenios-proximos">SALÕES CONVENIADOS MAIS PRÓXIMOS DE VOCÊ:</h5>

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.1237268657555!2d-49.284505935399665!3d-25.434126733786535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40ace042a9d%3A0x4f605bf9f2b75e58!2sCondom%C3%ADnio+Edif%C3%ADcio+Wall+Street+Center+-+Alameda+Dr.+Carlos+de+Carvalho%2C+771+-+Centro%2C+Curitiba+-+PR%2C+80430-100!5e0!3m2!1spt-BR!2sbr!4v1513706191138" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>

    </div>

</section>

<section class="section bg second-section" style="background-image:url('../assets/img/home/bg-02.png"></section>

<?php include_once( '../nossa-loja.php' ); ?>

<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>