<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo $dir_fixer; ?>assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../navbar-logado-funcionario.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/minha-conta.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    MEUS
                </h2>

                <h1>POSTS</h1>

            </div>

        </div>

    </div>

</div>

<!-- Start of Content -->

<section class="section contato">

    <div class="container">

        <div class="row has-post">

            <div class="col-md-8 col-md-offset-2 no-border">

                <div class="posts no-scroll">

                    <?php for($i=1; $i < 10; $i++){ ?>
                        <div class="post">

                            <div class="header">

                                <div class="avatar">

                                    <img src="<?php echo $dir_fixer; ?>assets/img/home/img%20(1).jpg" />

                                </div>

                                <div class="info">

                                    <h5>Pérola Saint</h5>
                                    <small>Espaço Andry Molina</small>

                                </div>

                                <h5 class="time">

                                    <i class="fa fa-clock-o"></i> 1h

                                </h5>

                                <div class="actions">

                                    <i class="fa fa-ellipsis-h"></i>

                                    <ul>

                                        <li>EDITAR POST</li>
                                        <li>EXCLUIR POST</li>

                                    </ul>

                                </div>

                                <div class="clear"></div>

                            </div>

                            <div class="body">

                                <div class="image">

                                    <img src="<?php echo $dir_fixer; ?>assets/img/home/img%20(1).jpg" class="img-responsive" />

                                </div>

                                <div class="content">

                                    <div class="actions">

                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-comments-o"></i>
                                        <p><small>72 curtidas</small></p>

                                    </div>

                                    <p class="comment">
                                        <strong>Pérola Saint</strong>
                                        Mechas lindas, realizadas hoje aqui no salão, gostou? <br />
                                        Entre em contato através do WhatsApp (41) 912345-1234.
                                    </p>

                                    <small class="view-all-comments">Ver todos os 12 comentários</small>

                                    <p class="comment">
                                        <strong>Joana da Silva</strong>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </p>

                                    <p class="comment">
                                        <strong>José Mário Silva</strong>
                                        Lorem ipsum dolor sit amet, adipiscing elit, dots.
                                    </p>

                                    <input type="text" class="form-control type-02" placeholder="Adicione um comentário…" />

                                </div>

                            </div>

                        </div>
                    <?php } ?>

                </div>

            </div>

        </div>

    </div>

</section>

<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>