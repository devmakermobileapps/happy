<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
<!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../navbar-logado.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/minha-conta.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        &nbsp;MINHA
                    </h2>

                    <h1>CONTA</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="section minha-conta">

        <div class="container">

            <table class="table-none">

                <tr>

                    <td>

                        <div class="text">

                            <h5>STATUS</h5>
                            <p class="green">ATIVO</p>

                        </div>

                    </td>

                    <td>

                        <div class="text">

                            <h5>VENCIMENTO DO CARTÃO</h5>
                            <p class="third">01/01/2020</p>

                        </div>

                    </td>

                    <td><a href="#">Histórico de serviços</a></td>

                    <td><a href="#">Visualizar clientes</a></td>

                </tr>

            </table>

            <div class="row">

                <div class="col-md-6 form-lg">

                    <h5>DADOS CORPORATIVOS</h5>

                    <div class="form-group">

                        <label>Nome da empresa</label>
                        <input type="text" class="form-control type-03 lg" />

                    </div>

                    <div class="form-group">

                        <label>Razão social</label>
                        <input type="text" class="form-control type-03 lg" />

                    </div>

                    <div class="form-group">

                        <label>CNPJ</label>
                        <input type="text" class="form-control type-03 lg" placeholder="00.000.000/0001-00" />

                    </div>

                    <div class="form-group">

                        <label>Telefone</label>
                        <input type="text" class="form-control type-03 lg" placeholder="(00) 00000-0000" />

                    </div>

                    <div class="form-group">

                        <label>Site</label>
                        <input type="text" class="form-control type-03 lg" placeholder="www.seusite.com.br" />

                    </div>

                </div>

                <div class="col-md-6 form-lg">

                    <h5>DADOS PESSOAIS</h5>

                    <div class="form-group">

                        <label>Nome do responsável pela empresa</label>
                        <input type="text" class="form-control type-03 lg" placeholder="000.000.000-00" />

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Cargo</label>
                            <input type="text" class="form-control type-03 lg" />

                        </div>

                        <div class="col-md-6">

                            <label>Gênero</label>

                            <select class="select2 type-03 lg form-control">
                                <option>Masculino</option>
                                <option>Feminino</option>
                            </select>

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Telefone fixo</label>
                            <input type="text" class="form-control type-03 lg" placeholder="(00) 0000-0000" />

                        </div>

                        <div class="col-md-6">

                            <label>Whatsapp</label>
                            <input type="text" class="form-control type-03 lg" placeholder="(00) 00000-0000" />

                        </div>

                    </div>

                    <div class="form-group">

                        <label>Telefone</label>
                        <input type="text" class="form-control type-03 lg" placeholder="(00) 00000-0000" />

                    </div>

                    <div class="form-group">

                        <label>E-mail</label>
                        <input type="text" class="form-control type-03 lg" placeholder="email@seuemail.com.br" />

                    </div>

                </div>

                <div class="col-md-6 form-lg">

                    <h5>ENDEREÇO</h5>

                    <div class="form-group">

                        <label>CEP</label>
                        <input type="text" class="form-control type-03 lg" placeholder="00.000-000" />

                    </div>

                    <div class="form-group">

                        <label>Endereço</label>
                        <input type="text" class="form-control type-03 lg" />

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Número</label>
                            <input type="text" class="form-control type-03 lg" />

                        </div>

                        <div class="col-md-6">

                            <label>Complemento</label>
                            <input type="text" class="form-control type-03 lg" />

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Bairro</label>
                            <input type="text" class="form-control type-03 lg" />

                        </div>

                        <div class="col-md-6">

                            <label>Cidade</label>
                            <input type="text" class="form-control type-03 lg" />

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>UF</label>
                            <input type="text" class="form-control type-03 lg" />

                        </div>

                        <div class="col-md-6">

                            <label>País</label>
                            <input type="text" class="form-control type-03 lg" />

                        </div>

                    </div>

                </div>

                <div class="col-md-6 form-lg">

                    <h5>&nbsp</h5>

                    <div class="form-group">

                        <label>Descrição da empresa (500 caracteres)</label>
                        <textarea class="form-control type-03 lg"></textarea>

                    </div>

                    <div class="form-group">

                        <label>Infraestrutura (500 caracteres)</label>
                        <textarea class="form-control type-03 lg"></textarea>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-3 right-v-center">

                            Fotos (Máx. 12)

                        </div>

                        <div class="col-md-9">


                            <ul class="images photos">

                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>

                            </ul>

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-3 right-v-center">

                            Banners (Máx. 3)

                        </div>

                        <div class="col-md-9">


                            <ul class="images banners">

                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><i class="fa fa-close third"></i><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>

                            </ul>

                        </div>

                    </div>

                </div>

                <div class="col-md-12 form-lg">

                    <h5>CADASTRO DE SERVIÇOS</h5>

                    <div class="form-group">

                        <div class="input-group type-03 lg">

                            <span class="input-group-addon">

                                <input type="radio" name="filter-1" id="filter-2" class="radio-default" />
                                <label for="filter-2"></label>

                            </span>

                            <p class="form-control-static">Serviços de cabelo/sobrancelha/manicure/micro pigmentação/estética/podólogo, dentre outros.</p>

                        </div>

                        <small class="help-block pull-right mudar-plano">Mudar plano.</small>

                    </div>

                </div>

                <div class="col-md-4 col-md-offset-8 m-t-20">

                    <button class="btn default btn-block lg">SALVAR DADOS</button>

                </div>

            </div>

        </div>

    </section>

	<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>