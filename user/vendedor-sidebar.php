        <div class="sidebar scrollable">

            <div class="form-group search">

                <div class="input-group">

                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span>

                    <input type="search" class="form-control type-01" placeholder="Procurar" />

                </div>

            </div>

            <ul class="menu">

                <a href="minha-conta-vendedor.php" class="menu-item">

                    <i class="material-icons">dashboard</i>

                    <h5>Minha conta</h5>

                </a>

                <a href="minha-conta-vendedor-produtos.php" class="menu-item active" data-submenu="perfumes">

                    <i class="material-icons">location_city</i>

                    <h5>Produtos</h5>

                </a>

                <a href="minha-conta-vendedor-novo-produto.php" class="menu-item" data-submenu="perfumes">

                    <i class="happy-icons"></i>

                    <h5>Adicionar produto</h5>

                </a>

                <a href="minha-conta-vendedor-historico-vendas.php" class="menu-item" data-submenu="perfumes">

                    <i class="material-icons">query_builder</i>

                    <h5>Histórico de vendas</h5>

                </a>

            </ul>

        </div>