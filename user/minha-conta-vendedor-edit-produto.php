<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
<!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../navbar-logado-vendedor.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/minha-conta.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        &nbsp;MINHA
                    </h2>

                    <h1>CONTA</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="nossa-loja">

        <?php include_once( 'vendedor-sidebar.php' ); ?>

        <div class="content header">

            <div class="row">

                <div class="col-md-6">

                    <p>Minha conta > Produtos > <strong>Adicionar produto</strong></p>

                </div>

            </div>

        </div>

        <div class="content scrollable">

            <div class="row">

                <div class="col-md-5">

                    <div class="fake-slider">

                        <div class="thumbnail">

                            <i class="fa fa-close"></i>

                            <img src="../assets/img/produto.jpg" alt="produto" />

                        </div>

                        <div class="thumbs">

                            <?php for($i=1; $i<5; $i++){ ?>
                            <div class="thumb">

                                <i class="fa fa-close"></i>

                                <img src="../assets/img/produto.jpg" alt="produto" />

                            </div>
                            <?php } ?>

                        </div>

                    </div>

                    <div class="form-group m-t-20">

                        <label>Categoria</label>
                        <input type="text" class="form-control type-03 lg" />

                    </div>

                </div>

                <div class="col-md-7">

                    <div class="form-group">

                        <label>Título</label>
                        <input type="text" class="form-control type-03 lg" placeholder="Escrever...">

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Preço</label>
                            <input type="text" class="form-control type-03 lg" placeholder="R$0,00">

                        </div>

                        <div class="col-md-6">

                            <label>Desconto</label>
                            <input type="text" class="form-control type-03 lg" placeholder="0%">

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Parcelamento em até</label>
                            <input type="text" class="form-control type-03 lg" placeholder="Escrever…">

                        </div>

                        <div class="col-md-6">

                            <label>Código</label>
                            <input type="text" class="form-control type-03 lg" placeholder="000000">

                        </div>

                    </div>
                    
                    <div class="form-group">

                        <label>Descrição</label>
                        <textarea rows="5" class="form-control type-03" placeholder="Escrever..."></textarea>
                        
                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <button class="btn third lg btn-block">CANCELAR</button>

                        </div>

                        <div class="col-md-6">

                            <button class="btn default lg btn-block">SALVAR</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <div class="modal type-01" id="produto_view-modal" role="dialog">

        <div class="modal-dialog modal-md">

            <div class="modal-content">

                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="slick slider-for">
                                <?php for($i=1; $i<=8; $i++){ ?>
                                    <div><img src="../assets/img/home/img%20(<?php echo $i; ?>).jpg" class="img-responsive" /></div>
                                <?php } ?>
                            </div>

                            <div class="slick slider-nav">
                                <?php for($i=1; $i<=8; $i++){ ?>
                                    <div><img src="../assets/img/home/img%20(<?php echo $i; ?>).jpg" class="img-responsive" /></div>
                                <?php } ?>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <h3 class="title">
                                Kit Aussie Shampoo 400ml +
                                Condicionador 400ml +
                                3 minute
                            </h3>

                            <span class="sku">AHU6798</span>

                            <p class="desc">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.
                            </p>

                            <label>Quantidade:</label>

                            <div class="form-group row">

                                <div class="col-md-3">

                                    <input type="number" class=" form-control type-03" />

                                </div>

                            </div>

                            <div class="form-group">

                                <button class="btn default btn-block btn-lg">ADICIONAR AO CARINHO</button>

                            </div>

                            <a href="carrinho.php" class="ver-carrinho">Ver carrinho</a>

                        </div>

                    </div>

                    <div class="container testimonial-group">

                        <div class="row text-center">

                            <?php for($i=1; $i<10; $i++){ ?>
                                <div class="col-xs-3">

                                    <div class="thumbnail">

                                        <span class="promotion">-30%</span>

                                        <div class="image">

                                            <img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="Produto" class="img-responsive" />

                                        </div>

                                        <div class="caption">

                                            <p class="desc">
                                                Kit Aussie Shampoo 400 ml +
                                                Condicionardor 400 ml + 3 minute
                                            </p>

                                            <h5 class="sku">AHU6798</h5>

                                            <h4 class="price">R$ 59,90</h4>

                                            <div class="credit-card">
                                                ou até 10x de R$5,90
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            <?php } ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

	<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>