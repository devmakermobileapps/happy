<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo $dir_fixer; ?>assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../navbar-logado-convenio.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/banner-vendedor.jpg');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;MEUS
                </h2>

                <h1>PEDIDOS</h1>

            </div>

        </div>

    </div>

</div>

<!-- Start of Content -->

<section class="section meus-pedidos">

    <div class="container p-v-40">

<!--        <div class="half-bg"><div><h3>MEUS PEDIDOS</h3></div></div>-->

        <table class="table">

            <thead>

            <tr>

                <th>PRODUTO</th>
                <th>PEDIDO</th>
                <th>DATA DO PEDIDO</th>
                <th>PREÇO</th>
                <th>STATUS</th>
                <th></th>

            </tr>

            </thead>

            <tbody>

            <tr>

                <td>

                    <div class="flex-v-center">

                        <div class="image">

                            <img src="../assets/img/produto.jpg" alt="produto" class="img-responsive">

                        </div>

                        <p>
                            Kit Aussie Shampoo 400ml +
                            Condicionador 400ml + 3 minute
                        </p>

                    </div>

                </td>

                <td>0054868741</td>

                <td>05/12/2017</td>

                <td>R$59,90</td>

                <td>PAGAMENTO APROVADO</td>

                <td><a href="#">Ver detalhes</a></td>

            </tr>

            </tbody>

        </table>

        <ul class="steps">
            <li class="step"><span href="#">●</span> <h3>PEDIDO ENTREGUE</h3> <small>05/12/2017</small></li>
            <li class="step"><span href="#">●</span> <h3>PEDIDO EM TRANSPORTE</h3> <small>05/12/2017</small></li>
            <li class="step"><span href="#">●</span> <h3>PREPARANDO PARA ENVIO</h3> <small>05/12/2017</small></li>
            <li class="step active"><span href="#">●</span> <h3>PAGAMENTO APROVADO</h3> <small>05/12/2017</small></li>
        </ul>

    </div>

</section>

<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>