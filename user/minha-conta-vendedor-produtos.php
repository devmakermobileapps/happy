,<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
<!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../navbar-logado-vendedor.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/minha-conta.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        &nbsp;MINHA
                    </h2>

                    <h1>CONTA</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="nossa-loja">

        <?php include_once( 'vendedor-sidebar.php' ); ?>

        <div class="content header">

            <div class="row">

                <div class="col-md-6">

                    <p>Minha conta</p>

                </div>

            </div>

        </div>

        <div class="content scrollable">

            <table class="table datatable table-hover">

                <thead>

                    <tr>

                        <th>PRODUTO</th>
                        <th>PREÇO</th>
                        <th>CÓDIGO</th>
                        <th>CATEGORIA</th>
                        <th></th>

                    </tr>

                </thead>

                <tbody>

                    <?php for($i=1; $i<23; $i++){ ?>
                    <tr>

                        <td><strong>Kit Aussie Shampoo 400 ml + Condicionardor 400 ml + 3 minute</strong></td>
                        <td>R$59,90</td>
                        <td>000135609</td>
                        <td>Cabelos</td>

                        <td>

                            <i onclick="window.location.href = 'minha-conta-vendedor-edit-produto.php';" class="material-icons green">mode_edit</i>
                            <i class="material-icons third">clear</i>

                        </td>

                    </tr>
                    <?php } ?>

                </tbody>

            </table>

        </div>

    </section>

	<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>