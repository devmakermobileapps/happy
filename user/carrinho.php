<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo $dir_fixer; ?>assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../navbar-logado-convenio.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/banner-vendedor.jpg');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;&nbsp
                </h2>

                <h1>CARRINHO</h1>

            </div>

        </div>

    </div>

</div>

<!-- Start of Content -->

<section class="section carrinho p-v-40">

    <div class="container">

<!--        <div class="half-bg"><div><h3>CARRINHO</h3></div></div>-->

        <table class="table">

            <thead>

            <tr>

                <th>PRODUTO</th>
                <th>QT.</th>
                <th>PREÇO</th>
                <th>SUBTOTAL</th>
                <th></th>

            </tr>

            </thead>

            <tbody>

            <?php for($i=0; $i<2; $i++){ ?>
            <tr>

                <td>

                    <div class="flex-v-center">

                        <div class="image">

                            <img src="../assets/img/produto.jpg" alt="produto" class="img-responsive">

                        </div>

                        <p>Kit Aussie Shampoo 400ml + Condicionador 400ml + 3 minute</p>

                    </div>

                </td>

                <td>

                    <input type="number" class="form-control type-03 input-qt" />

                </td>

                <td>R$59,90</td>

                <td>R$59,90</td>

                <td>
                    <i class="material-icons">clear</i>
                </td>

            </tr>
            <?php } ?>

            </tbody>

        </table>

        <div class="row">

            <div class="col-md-6">

                <div class="form-group row">

                    <div class="col-md-4">

                        <label>CEP:</label>
                        <input type="text" class="form-control normal type-03" />

                    </div>

                    <div class="col-md-2">

                        <label>&nbsp</label>
                        <input type="text" class="form-control normal type-03" />

                    </div>

                    <div class="col-md-6">

                        <label>&nbsp</label>
                        <button class="btn default normal btn-block">CALCULAR FRETE</button>
                        <span class="help-block">NÃO SEI MEU CEP</span>

                    </div>

                </div>

            </div>

            <div class="col-md-5 col-md-offset-1 text-right">

                <h3 class="default">FRETE: GRÁTIS</h3>
                <p>6 A 7 DIAS ÚTEIS</p>

            </div>

        </div>

        <div class="row">

            <div class="col-md-5 col-md-offset-7 text-right">

                <p><strong>SUBTOTAL: R$119,80 </strong></p>
                <h3 class="default">TOTAL: R$119,80 </h3>

            </div>

        </div>

        <button class="btn default lg pull-right">FINALIZAR COMPRA</button>

        <button class="btn default outline lg pull-right">CONTINUAR COMPRANDO</button>

    </div>

</section>

<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>