<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../header-modais.php' ); ?>

<?php include_once( '../navbar-logado-funcionario.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/minha-conta.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    ADICIONAR
                </h2>

                <h1>POST</h1>

            </div>

        </div>

    </div>

</div>

<!-- Start of Content -->

<section class="section default type-01 contato">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <input type="file" class="dropify" data-height="450" />

            </div>

            <div class="col-md-6">

                <div class="form-group">

                    <label>Marcar pessoas</label>
                    <input type="text" class="form-control type-03 lg" placeholder="@nome" />

                </div>

                <div class="form-group">

                    <label>Adicionar localização</label>
                    <input type="text" class="form-control type-03 lg" placeholder="seuemail@gmail.com.br" />

                </div>

                <div class="form-group">

                    <label>Escreva uma legenda</label>
                    <textarea class="form-control type-03" rows="5" placeholder="Máx. 250 caracteres"></textarea>

                </div>

                <div class="form-group">

                    <button class="btn default btn-block lg">Enviar</button>

                </div>

            </div>

        </div>

    </div>

</section>

<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>