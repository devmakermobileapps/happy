<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo $dir_fixer; ?>assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../navbar-logado-convenio.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/banner-vendedor.jpg');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;MEUS
                </h2>

                <h1>PEDIDOS</h1>

            </div>

        </div>

    </div>

</div>

<!-- Start of Content -->

<section class="section meus-pedidos p-v-40">

    <div class="container">

<!--        <div class="half-bg"><div><h3>MEUS PEDIDOS</h3></div></div>-->

        <table class="table">

            <thead>

            <tr>

                <th>PRODUTO</th>
                <th>PEDIDO</th>
                <th>DATA DO PEDIDO</th>
                <th>PREÇO</th>
                <th>STATUS</th>
                <th>
                    <select class="select2 form-control type-02">

                        <option selected>Aprovados</option>
                        <option>Encerrados</option>
                        <option>Aguardando envio</option>
                        <option>Em transporte</option>

                    </select>
                </th>

            </tr>

            </thead>

            <tbody>

            <?php for($i=0; $i<4; $i++){ ?>
            <tr>

                <td>

                    <div class="flex-v-center">

                        <div class="image">

                            <img src="../assets/img/produto.jpg" alt="produto" class="img-responsive">

                        </div>

                        <p>
                            Kit Aussie Shampoo 400ml +
                            Condicionador 400ml + 3 minute
                        </p>

                    </div>

                </td>

                <td>0054868741</td>

                <td>05/12/2017</td>

                <td>R$59,90</td>

                <td>PAGAMENTO APROVADO</td>

                <td><a href="detalhes-pedido.php">Ver detalhes</a></td>

            </tr>
            <?php } ?>

            </tbody>

        </table>

        <div class="pagination">

            <ul>

                <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                <li><a href="#"><i class="fa fa-angle-left"></i></a></li>

                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">18</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>

            </ul>

        </div>

    </div>

</section>

<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>