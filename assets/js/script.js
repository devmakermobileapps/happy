$(window).scroll(function(){

    var window_y = $(this).scrollTop();

    if(window_y >= 230){

        $('.custom-navbar').css({
            'background-color': '#58b7bf'
        })

        $('.custom-navbar .btn').css({
            'background-color': 'transparent',
            'border': '1px solid #fff'
        })

    }else{
        $('.custom-navbar').css({
            'background-color': 'transparent'
        })

        $('.custom-navbar .btn').css({
            'background-color': '#58b7bf',
            'border': '1px solid transparent'
        })
    }

})

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    $('.posts .post .header .fa-ellipsis-h').click(function(){

        $(this).next().toggle();

    })

    $('.dropify').dropify();

    $('#btn-cadastro-choice').click(function(){

        var choice = $('#cadastro-choice').val();
        window.location.href = choice;

    })

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });

    var selected_menu = $('.sidebar .menu .menu-item.active').attr('data-submenu');
    $('.sidebar .menu .submenu#' + selected_menu).show();

    $('.sidebar .menu-item').click(function(){

        if($(this).hasClass('active')){

            $('.sidebar .menu .submenu').slideUp(300);
            $('.sidebar .menu-item').removeClass('active');

        }else{

            var submenu = $(this).attr('data-submenu');
            $('.sidebar .menu .submenu').slideUp(300);
            $('#' + submenu).slideToggle(300);

            $('.sidebar .menu-item').removeClass('active');
            $(this).addClass('active');

        }

    })


    $('.forgot-password').click(function(){

        $('.modal#login-modal').modal('toggle');

    })

    $('select').select2({
        width: '100%'
    });

    $('.select2-keepOpen').select2({
        width: '100%',
        closeOnSelect: false
    });

    $('.notifications').click(function(){

        $('.notifications .nots').fadeToggle(300);

    })

    //Anuncios Steps

    // Mostra o step selecionado no carregamento do html

    var current_step = $('.steps-table td.active').attr('data-step');
    $('.anuncios .anuncios-content').hide();
    $('.anuncios .anuncios-content#' + current_step).show();

    $('.steps-table td').click(function(){

        var step = $(this).attr('data-step');

        // Esconde todas as ambas e então mostra apenas a que foi selecionada.
        $('.anuncios .anuncios-content').hide();
        $('.anuncios .anuncios-content#' + step).show();

        // Retira o active de todos os steps e então coloca apenas no que foi selecionado.
        $('.steps-table td').removeClass('active');
        $(this).addClass('active');

    })

    // Agenda

    $('.datetimepicker').datepicker();

    // Transforma .datable em DataTable
    $('.datatable').DataTable({
        colReorder: true,
        responsive: true,
        bFilter: false,
        bInfo: false,
        "bLengthChange": false,
        "language": {
            "paginate": {
                "previous": "<i class='fa fa-angle-double-left'></i>",
                "next": "<i class='fa fa-angle-double-right'></i>"
            }
        },
        "dom": 'rtp'
    });

    $('.chat').ready(function(){

        $( ".chat > div" ).after( "<div class='clear'></div>" );

    })

    var os_current_step_modal = $('.step.active').attr('data-modal');
    $('.step .material-icons').attr('data-target', "#" + os_current_step_modal);

    $('.steps .step').click(function(){

        // var os_current_step_modal = $('.step.active').attr('data-modal');
        // $('.step .material-icons').attr('data-target', "#" + os_current_step_modal);
        //
        // $('.steps .step').removeClass('active');
        // $(this).addClass('active');

    })

    montaScroll(true);
});

function montaScroll(destruir) {

    if(destruir){
        $('.body-content.scrollable').slimScroll({
            destroy: true
        });
    }

    $('.body-content.scrollable').slimScroll({
        start: 'bottom',
        allowPageScroll: true,
        height: 350
    });

}
