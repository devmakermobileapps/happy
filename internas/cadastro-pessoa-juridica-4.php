<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
<!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../header-modais.php' ); ?>

    <?php include_once( '../navbar.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/cadastrese.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        &nbsp;
                    </h2>

                    <h1>Cadastre-se</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="section pessoa-juridica-cadastre-se">

        <div class="container">

            <div class="row">

                <div class="col-md-6 form-lg">

                    <h5>DADOS CORPORATIVOS</h5>

                    <div class="form-group">

                        <label>Nome da empresa</label>
                        <p class="form-control-static type-03 lg">Lorem ipsum</p>

                    </div>

                    <div class="form-group">

                        <label>Razão social</label>
                        <p class="form-control-static type-03 lg">Lorem ipsum</p>

                    </div>

                    <div class="form-group">

                        <label>CNPJ</label>
                        <p class="form-control-static type-03 lg">00.000.000/0001-00</p>

                    </div>

                    <div class="form-group">

                        <label>Telefone</label>
                        <p class="form-control-static type-03 lg">(00) 00000-0000</p>

                    </div>

                    <div class="form-group">

                        <label>Site</label>
                        <p class="form-control-static type-03 lg">www.seusite.com.br</p>

                    </div>

                </div>

                <div class="col-md-6 form-lg">

                    <h5>DADOS PESSOAIS</h5>

                    <div class="form-group">

                        <label>Nome do responsável pela empresa</label>
                        <p class="form-control-static type-03 lg">000.000.000-00</p>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Cargo</label>
                            <p class="form-control-static type-03 lg"></p>

                        </div>

                        <div class="col-md-6">

                            <label>Gênero</label>

                            <p class="form-control-static type-03 lg">Feminino</p>

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Telefone fixo</label>
                            <p class="form-control-static type-03 lg">(00) 0000-0000</p>

                        </div>

                        <div class="col-md-6">

                            <label>Whatsapp</label>
                            <p class="form-control-static type-03 lg">(00) 00000-0000</p>

                        </div>

                    </div>

                    <div class="form-group">

                        <label>Telefone</label>
                        <p class="form-control-static type-03 lg">(00) 00000-0000</p>

                    </div>

                    <div class="form-group">

                        <label>E-mail</label>
                        <p class="form-control-static type-03 lg">email@seuemail.com.br</p>

                    </div>

                </div>

                <div class="col-md-6 form-lg">

                    <h5>ENDEREÇO</h5>

                    <div class="form-group">

                        <label>CEP</label>
                        <p class="form-control-static type-03 lg">00.000-000</p>

                    </div>

                    <div class="form-group">

                        <label>Endereço</label>
                        <p class="form-control-static type-03 lg">Rua Lorem Ipsum</p>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Número</label>
                            <p class="form-control-static type-03 lg">1234</p>

                        </div>

                        <div class="col-md-6">

                            <label>Complemento</label>
                            <p class="form-control-static type-03 lg">Lorem</p>

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Bairro</label>
                            <p class="form-control-static type-03 lg">Lorem</p>

                        </div>

                        <div class="col-md-6">

                            <label>Cidade</label>
                            <p class="form-control-static type-03 lg">Lorem</p>

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>UF</label>
                            <p class="form-control-static type-03 lg">Lorem</p>

                        </div>

                        <div class="col-md-6">

                            <label>País</label>
                            <p class="form-control-static type-03 lg"></p>

                        </div>

                    </div>

                    <div class="form-group"></div>

                </div>

                <div class="col-md-6 form-lg">

                    <h5>&nbsp</h5>

                    <div class="form-group">

                        <label>Descrição da empresa (500 caracteres)</label>
                        <p class="form-control-static type-03 extra-lg">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
                        </p>

                    </div>

                    <div class="form-group">

                        <label>Infraestrutura (500 caracteres)</label>
                        <p class="form-control-static type-03 extra-lg">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
                        </p>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-3 right-v-center">

                            Fotos (Máx. 12)

                        </div>

                        <div class="col-md-9">

                            <ul class="images photos">

                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>

                            </ul>

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-3 right-v-center">

                            Banners (Máx. 3)

                        </div>

                        <div class="col-md-9">

                            <ul class="images banners">

                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>
                                <li><img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="produto"></li>

                            </ul>

                        </div>

                    </div>

                </div>

                <div class="col-md-12 form-lg">

                    <h5>CADASTRO DE SERVIÇOS</h5>

                    <div class="form-group">

                        <div class="input-group type-03 lg">

                            <span class="input-group-addon">

                                <input type="radio" name="filter-1" id="filter-1" class="radio-default" checked />
                                <label for="filter-1"></label>

                            </span>

                            <p class="form-control-static type-03 lg">Serviços de cabelo/sobrancelha/manicure/micro pigmentação/estética/podólogo, dentre outros.</p>

                        </div>

                    </div>

                </div>

                <div class="col-md-3 col-md-offset-6 m-t-20">

                    <button class="btn default btn-block lg">Voltar</button>

                </div>

                <div class="col-md-3 m-t-20">

                    <button class="btn default btn-block lg" data-toggle="modal" data-target="#cadastro_sucesso-modal">Confirmar</button>

                </div>

            </div>

        </div>

    </section>

	<!-- End of Content -->

    <!-- Modal -->

    <div class="modal type-01" id="cadastro_sucesso-modal" role="dialog">

        <div class="modal-dialog modal-md">

            <div class="modal-content">

                <div class="modal-body">

                    <h3>Pronto, seu cadastro foi efetuado!</h3>

                    <p>Confira na tela a seguir seus dados cadastrados!</p>

                </div>

                <div class="modal-footer no-padding flex">

                    <button class="btn third full" class="close" data-dismiss="modal">Cancelar</button>
                    <button onclick="window.location.href = '../user/minha-conta.php';" class="btn default full">Continuar</button>

                </div>

            </div>

        </div>

    </div>

<?php include_once( '../footer.php' ); ?>