<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <meta http-equiv="refresh" content="1" />
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../header-modais.php' ); ?>

    <?php include_once( '../navbar.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/cadastrese.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        &nbsp;
                    </h2>

                    <h1>Cadastre-se</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="section pessoa-juridica-cadastre-se">

        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <div class="row">

                    <div class="col-md-4 logo">

                        <div class="image">

                            <img src="<?php echo $dir_fixer; ?>assets/img/logo-conve-nios-2.png" class="img-responsive" />

                        </div>

                    </div>

                    <div class="col-md-8">

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>

                        <button onclick="window.location.href = 'cadastro-pessoa-juridica-1.php';" class="btn default btn-block">CLIQUE AQUI PARA INICIAR SEU CADASTRO</button>

                    </div>

                </div>

            </div>

        </div>

    </section>

	<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>