<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../header-modais.php' ); ?>

<?php include_once( '../navbar.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/planos.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;COMO
                </h2>

                <h1>FUNCIONA</h1>

            </div>

        </div>

    </div>

</div>

    <!-- Start of Content -->

    <section class="section default type-01 planos">

        <div class="container">

            <p>
                Ao assinar o Convênio Happy, garantimos 15% de desconto em todos os serviços da nossa rede credenciada: Salões de Beleza, Clínicas de Estética, Depilação, Sobrancelhas, Maquiagem, Pilates, Manicure e Pedicure, Tatuagens, Barbearia.
            </p>

            <div class="row">

                <div class="col-md-4 col-md-offset-2 flex-all-center">

                    <div class="plano">

                        <h5 class="title">PLANO OURO</h5>

                        <ul class="items">

                            <li>Salão de Beleza</li>
                            <li>Barbearia</li>

                        </ul>

                        <h4 class="price">R$19,90</h4>

                    </div>

                </div>

                <div class="col-md-4 flex-all-center">

                    <div class="plano">

                        <h5 class="title">PLANO OURO PLUS</h5>

                        <ul class="items">

                            <li>Salão de Beleza</li>
                            <li>Barbearia</li>
                            <li>Estética</li>
                            <li>Pilates</li>
                            <li>Tatuagem</li>

                        </ul>

                        <h4 class="price">R$29,90</h4>

                    </div>

                </div>

            </div>

        </div>

        <div class="form-group row">

            <div class="col-md-4 col-md-offset-4">

                <button class="btn default lg btn-block">CRIAR CADASTRO</button>

            </div>

        </div>

    </section>

    <!-- End of Content -->

<?php include_once( '../footer.php' ); ?>