<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../header-modais.php' ); ?>

    <?php include_once( '../navbar.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/perfil-profissional.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        PERFIL
                    </h2>

                    <h1>PROFISSIONAL</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="section contato">

        <div class="container">

            <div class="col-md-5">

                <div class="image">

                    <img src="../assets/img/andreia.png" class="img-responsive" />

                </div>

            </div>

            <div class="col-md-7">

                <div class="form-group">

                    <label>Nome</label>
                    <p class="form-control-static type-02">Joana Silva</p>

                </div>

                <div class="form-group">

                    <label>Salão</label>
                    <p class="form-control-static type-02">Espaço Andry Molina</p>

                </div>

                <div class="form-group">

                    <label>Cargo</label>
                    <p class="form-control-static type-02">Lorem</p>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">

                        <label>Telefone do salão</label>
                        <p class="form-control-static type-02">(00) 00000-0000</p>

                    </div>

                    <div class="col-md-6">

                        <label>E-mail</label>
                        <p class="form-control-static type-02">email@seuemail.combr</p>

                    </div>

                </div>

                <div class="form-group">

                    <label>Biografia</label>

                    <p class="form-control-static type-02">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat ex ea commodo conf.
                    </p>

                </div>

                <div class="form-group">

                    <button class="btn default lg btn-block">Avaliar</button>

                </div>

            </div>

        </div>

    </section>

    <section class="section grey contato">

        <div class="half-bg"><div><h3>LINHA DO TEMPO</h3></div></div>

        <div class="row has-post">

            <div class="col-md-6 col-md-offset-3">

                <div class="posts">

                    <?php for($i=1; $i < 10; $i++){ ?>
                        <div class="post">

                            <div class="header">

                                <div class="avatar">

                                    <img src="../assets/img/home/img%20(1).jpg" />

                                </div>

                                <div class="info">

                                    <h5>Pérola Saint</h5>
                                    <small>Espaço Andry Molina</small>

                                </div>

                                <h5 class="time"><i class="fa fa-clock-o"></i> 1h</h5>

                                <div class="clear"></div>

                            </div>

                            <div class="body">

                                <div class="image">

                                    <img src="../assets/img/home/img%20(1).jpg" class="img-responsive" />

                                </div>

                                <div class="content">

                                    <div class="actions">

                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-comments-o"></i>
                                        <p><small>72 curtidas</small></p>

                                    </div>

                                    <p class="comment">
                                        <strong>Pérola Saint</strong>
                                        Mechas lindas, realizadas hoje aqui no salão, gostou? <br />
                                        Entre em contato através do WhatsApp (41) 912345-1234.
                                    </p>

                                    <small class="view-all-comments">Ver todos os 12 comentários</small>

                                    <p class="comment">
                                        <strong>Joana da Silva</strong>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </p>

                                    <p class="comment">
                                        <strong>José Mário Silva</strong>
                                        Lorem ipsum dolor sit amet, adipiscing elit, dots.
                                    </p>

                                    <input type="text" class="form-control type-02" placeholder="Adicione um comentário…" />

                                </div>

                            </div>

                        </div>
                    <?php } ?>

                </div>

            </div>

        </div>

    </section>

	<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>