<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../header-modais.php' ); ?>

<?php include_once( '../navbar.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/cadastrese.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;
                </h2>

                <h1>CADASTRE-SE</h1>
            </div>

        </div>

    </div>

</div>

<!-- Start of Content -->

<section class="section pessoa-juridica-cadastre-se">

    <div class="container">

        <div class="row">

            <div class="col-md-6 form-lg">

                <h5>DADOS CORPORATIVOS</h5>

                <div class="form-group">

                    <label>Nome da empresa</label>
                    <p class="form-control-static type-03 lg">Lorem ipsum</p>

                </div>

                <div class="form-group">

                    <label>Razão social</label>
                    <p class="form-control-static type-03 lg">Lorem ipsum</p>

                </div>

                <div class="form-group">

                    <label>CNPJ</label>
                    <p class="form-control-static type-03 lg">00.000.000/0001-00</p>

                </div>

                <div class="form-group">

                    <label>Telefone</label>
                    <p class="form-control-static type-03 lg">(00) 00000-0000</p>

                </div>

                <div class="form-group">

                    <label>Site</label>
                    <p class="form-control-static type-03 lg">www.seusite.com.br</p>

                </div>

            </div>

            <div class="col-md-6 form-lg">

                <h5>DADOS PESSOAIS</h5>

                <div class="form-group">

                    <label>Nome do responsável pela empresa</label>
                    <p class="form-control-static type-03 lg">000.000.000-00</p>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">

                        <label>Cargo</label>
                        <p class="form-control-static type-03 lg"></p>

                    </div>

                    <div class="col-md-6">

                        <label>Gênero</label>

                        <p class="form-control-static type-03 lg">Feminino</p>

                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">

                        <label>Telefone fixo</label>
                        <p class="form-control-static type-03 lg">(00) 0000-0000</p>

                    </div>

                    <div class="col-md-6">

                        <label>Whatsapp</label>
                        <p class="form-control-static type-03 lg">(00) 00000-0000</p>

                    </div>

                </div>

                <div class="form-group">

                    <label>Telefone</label>
                    <p class="form-control-static type-03 lg">(00) 00000-0000</p>

                </div>

                <div class="form-group">

                    <label>E-mail</label>
                    <p class="form-control-static type-03 lg">email@seuemail.com.br</p>

                </div>

            </div>

            <div class="col-md-3 col-md-offset-6 m-t-20">

                <button class="btn third btn-block lg">Voltar</button>

            </div>

            <div class="col-md-3 m-t-20">

                <button class="btn default btn-block lg" data-toggle="modal" data-target="#cadastro_sucesso-modal">Confirmar</button>

            </div>

        </div>

    </div>

</section>

<!-- End of Content -->

<!-- Modal -->

<div class="modal type-01" id="cadastro_sucesso-modal" role="dialog">

    <div class="modal-dialog modal-md">

        <div class="modal-content">

            <div class="modal-body">

                <h3>Pronto, seu cadastro foi efetuado!</h3>

                <p>Confira na tela a seguir seus dados cadastrados!</p>

            </div>

            <div class="modal-footer no-padding flex">

                <button class="btn third full" class="close" data-dismiss="modal">Cancelar</button>
                <button onclick="window.location.href = '../user/minha-conta-vendedor.php';" class="btn default full">Continuar</button>

            </div>

        </div>

    </div>

</div>

<?php include_once( '../footer.php' ); ?>