<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo $dir_fixer; ?>assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../header-modais.php' ); ?>

    <?php include_once( '../navbar.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/cadastrese.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        &nbsp
                    </h2>

                    <h1>CADASTRE-SE</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="section">

        <div class="row pessoa-fisica-cadastre-se">

            <div class="col-md-6 lojas p-v-40 form-lg">

                <div class="form-group row">

                    <div class="col-md-5">

                        <div class="image">

                            <img src="../assets/img/andreia.png" alt="andreia" class="img-responsive" />

                        </div>

                    </div>

                    <div class="col-md-7 info">

                        <h5>STATUS</h5>
                        <h5 class="green">ATIVO</h5>
                        <h5>VENCIMENTO DA ASSINATURA</h5>
                        <h5 class="third">01/01/2020</h5>

                    </div>

                </div>

                <div class="form-group">

                    <label>Nome completo</label>
                    <p class="form-control-static type-03 lg">Lorem ipsum</p>

                </div>

                <div class="form-group">

                    <label>CPF</label>
                    <p class="form-control-static type-03 lg">000.000.000-00</p>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">

                        <label>Estado</label>

                        <p class="form-control-static type-03 lg">&nbsp</p>

                    </div>

                    <div class="col-md-6">

                        <label>Cidade</label>

                        <p class="form-control-static type-03 lg">&nbsp</p>

                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">

                        <label>Telefone</label>
                        <p class="form-control-static type-03 lg">(00) 00000-0000</p>

                    </div>

                    <div class="col-md-6">

                        <label>E-mail</label>
                        <p class="form-control-static type-03 lg">email@seuemail.com.br</p>

                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-6">

                        <label>Gênero</label>

                        <p class="form-control-static type-03 lg">&nbsp</p>

                    </div>

                    <div class="col-md-6">

                        <label>Data de nascimento</label>
                        <p class="form-control-static type-03 lg">dd/mm/aaaa</p>

                    </div>

                </div>

                <div class="form-group">

                    <label>Senha</label>
                    <p class="form-control-static type-03 lg">senhateste123</p>

                </div>

                <h5>PLANO CONTRATADO</h5>

                <div class="form-group">

                    <div class="input-group type-03 lg m-v-10">

                            <span class="input-group-addon">

                                <input type="radio" name="filter-1" id="filter-1" class="radio-default" checked />
                                <label for="filter-1"></label>

                            </span>

                        <p class="form-control-static">Serviços de cabelo/sobrancelhas/manicure.</p>

                    </div>

                </div>

                <div class="form-group">

                    <button class="btn default btn-lg btn-block" data-toggle="modal" data-target="#cadastro_sucesso-modal">CONFIRMAR</button>

                </div>

            </div>

            <div class="col-md-6">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.1237268657555!2d-49.284505935399665!3d-25.434126733786535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40ace042a9d%3A0x4f605bf9f2b75e58!2sCondom%C3%ADnio+Edif%C3%ADcio+Wall+Street+Center+-+Alameda+Dr.+Carlos+de+Carvalho%2C+771+-+Centro%2C+Curitiba+-+PR%2C+80430-100!5e0!3m2!1spt-BR!2sbr!4v1513706191138" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>

        </div>

    </section>

	<!-- End of Content -->

    <!-- Modal -->

    <div class="modal type-01" id="cadastro_sucesso-modal" role="dialog">

        <div class="modal-dialog modal-md">

            <div class="modal-content">

                <div class="modal-body">

                    <h3>Pronto, seu cadastro foi efetuado!</h3>

                    <p>Confira na tela a seguir seus dados cadastrados!</p>

                </div>

                <div class="modal-footer no-padding flex">

                    <button class="btn third full" class="close" data-dismiss="modal">Cancelar</button>
                    <button onclick="window.location.href = '../user/minha-conta-convenio.php';" class="btn default full">Continuar</button>

                </div>

            </div>

        </div>

    </div>

<?php include_once( '../footer.php' ); ?>