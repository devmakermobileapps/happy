<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../header-modais.php' ); ?>

<?php include_once( '../navbar.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/como-funciona.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;COMO
                </h2>

                <h1>FUNCIONA</h1>

            </div>

        </div>

    </div>

</div>

    <!-- Start of Content -->

<section class="section default type-01 sobre">

    <div class="container">

        <div class="row">

            <div class="col-md-5">

                <div class="image">

                    <img src="../assets/img/juliana.png" alt="mulher sobre" class="img-resposnive" />

                </div>

            </div>

            <div class="col-md-7">

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    <br /><br />
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>

            </div>

        </div>


        <div class="half-bg"><div><h3>VANTAGENS</h3></div></div>

        <div class="container">

            <div class="row steps home-steps">

                <div class="col-md-4" style="background-image:url(<?php echo $dir_fixer; ?>assets/img/1.png);">

                    <div class="row">

                        <div class="col-md-9 col-md-offset-3">

                            <p>
                                Faça seu cadastro, adquira a assinatura anual e ganhe na hora um produto da Keune e retire em um dos nossos pontos.
                            </p>

                        </div>

                    </div>

                </div>

                <div class="col-md-4" style="background-image:url(<?php echo $dir_fixer; ?>assets/img/2.png);">

                    <div class="row">

                        <div class="col-md-9 col-md-offset-3">

                            <p>
                                Você aproveita o desconto em centenas de serviços que os salões de beleza e clínicas de estéticas conveniaos oferecem.
                            </p>

                        </div>

                    </div>

                </div>

                <div class="col-md-4" style="background-image:url(<?php echo $dir_fixer; ?>assets/img/3.png);">

                    <div class="row">

                        <div class="col-md-9 col-md-offset-3">

                            <p>
                                São 15% de descontos nos principais Salões de Beleza e Clínica de Estética em Curitiba e Região Metropolitana.
                            </p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

    <!-- End of Content -->

<?php include_once( '../footer.php' ); ?>