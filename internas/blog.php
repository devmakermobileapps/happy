<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../header-modais.php' ); ?>

<?php include_once( '../navbar.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/blog.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;
                </h2>

                <h1>BLOG</h1>

            </div>

        </div>

    </div>

</div>

    <!-- Start of Content -->

    <section class="section default type-01 blog">

        <div class="container">

            <div class="row">

                <?php for($i=1; $i<8; $i++){ ?>
                <div class="col-md-4">
                    
                    <div class="thumbnail">
                        
                        <div class="image" style="background-image: url('<?php echo $dir_fixer; ?>assets/img/home/img%20(<?php echo $i; ?>).jpg');">

                            <div class="overlay">
                                <h3>
                                    LOREM IPSUM DOLOR
                                    ASIMET
                                </h3>
                            </div>

                        </div>

                        <div class="caption">

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                            </p>

                            <a href="post.php" class="read-more">+ Ler mais...</a>

                        </div>
                        
                    </div>
                    
                </div>
                <?php } ?>
                
            </div>

            <div class="pagination">

                <ul>

                    <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                    <li><a href="#"><i class="fa fa-angle-left"></i></a></li>

                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">18</a></li>

                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>

                </ul>

            </div>

        </div>

    </section>

    <!-- End of Content -->

<?php include_once( '../footer.php' ); ?>