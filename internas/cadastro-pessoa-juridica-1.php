<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <meta http-equiv="refresh" content="1" />
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../header-modais.php' ); ?>

    <?php include_once( '../navbar.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/cadastrese.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        &nbsp;
                    </h2>

                    <h1>Cadastre-se</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="section pessoa-juridica-cadastre-se">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <label>LEIA E ACEITE O CONTRATO ABAIXO:</label>

                    <div class="read-terms type-03">

                        <div class="scrollable">

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                <br />
                                <br />
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </p>

                        </div>

                    </div>

                    <div class="form-group pull-right">

                        <input type="checkbox" id="filter-1" class="checkbox-default" />
                        <label for="filter-1">LI E ACEITO TODAS AS CLÁUSULAS DO CONTRATO</label>

                    </div>

                </div>

                <div class="col-md-4 col-md-offset-8">

                    <button onclick="window.location.href = 'cadastro-pessoa-juridica-2.php';" class="btn default btn-block">IR PARA PRÓXIMA ETAPA</button>

                </div>

            </div>

        </div>

    </section>

	<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>