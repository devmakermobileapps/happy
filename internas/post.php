<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../header-modais.php' ); ?>

<?php include_once( '../navbar.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/post.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;LOREM IPSUM
                </h2>

                <h1>DOLOR ASIMET</h1>

            </div>

        </div>

    </div>

</div>

    <!-- Start of Content -->

    <section class="section default type-01 planos">

        <div class="container">

            <img src="<?php echo $dir_fixer; ?>assets/img/post-thumb.png" alt="post thumb" class="img-responsive" />

            <article>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                <br />
                <br />
                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
            </article>

        </div>

    </section>

    <!-- End of Content -->

<?php include_once( '../footer.php' ); ?>