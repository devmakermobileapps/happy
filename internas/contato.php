<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../header-modais.php' ); ?>

<?php include_once( '../navbar.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/contato.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;
                </h2>

                <h1>CONTATO</h1>

            </div>

        </div>

    </div>

</div>

    <!-- Start of Content -->

    <section class="section default type-01 contato">

        <div class="container">

            <div class="row">

                <div class="col-md-6">

                    <div class="row">

                        <div class="col-md-4">

                            <div class="image">

                                <img src="../assets/img/facebook.png" alt="facebook" class="img-responsive" />

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="image">

                                <img src="../assets/img/twitter.png" alt="twitter" class="img-responsive" />

                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="image">

                                <img src="../assets/img/instagram.png" alt="instagram" class="img-responsive" />

                            </div>

                        </div>

                    </div>

                    <p>
                        Rua Lorem, 240 - Lorem Ipsum - PR <br />
                        (00) 0 0000-0000 <br />
                        (00) 0000-0000 <br />
                        atendimento@happy.com.br <br />
                    </p>

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.1237268657555!2d-49.284505935399665!3d-25.434126733786535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40ace042a9d%3A0x4f605bf9f2b75e58!2sCondom%C3%ADnio+Edif%C3%ADcio+Wall+Street+Center+-+Alameda+Dr.+Carlos+de+Carvalho%2C+771+-+Centro%2C+Curitiba+-+PR%2C+80430-100!5e0!3m2!1spt-BR!2sbr!4v1513706191138" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

                </div>

                <div class="col-md-6">

                    <div class="form-group">

                        <label>Nome</label>
                        <input type="text" class="form-control type-03 lg" />

                    </div>

                    <div class="form-group">

                        <label>E-mail</label>
                        <input type="text" class="form-control type-03 lg" placeholder="seuemail@gmail.com.br" />

                    </div>

                    <div class="form-group">

                        <label>Número</label>
                        <input type="text" class="form-control type-03 lg" placeholder="(00) 00000-0000" />

                    </div>

                    <div class="form-group">

                        <label>Mensagem</label>
                        <textarea class="form-control type-03" rows="2"></textarea>

                    </div>

                    <div class="form-group">

                        <button class="btn default btn-block lg">Enviar</button>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!-- End of Content -->

<?php include_once( '../footer.php' ); ?>