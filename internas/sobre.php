<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

<!-- Modal -->

<?php include_once( '../header-modais.php' ); ?>

<?php include_once( '../navbar.php' ); ?>

<div class="jumbotron" style="background-image:url('../assets/img/quem-somos.png');">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <h2>
                    &nbsp;QUEM
                </h2>

                <h1>SOMOS</h1>

            </div>

        </div>

    </div>

</div>

    <!-- Start of Content -->

    <section class="section default type-01 sobre">

        <div class="container">

            <div class="row">

                <div class="col-md-6">

                    <div class="image">

                        <img src="../assets/img/mulher-sobre.png" alt="mulher sobre" class="img-resposnive" />

                    </div>

                </div>

                <div class="col-md-6">

                    <p>
                        O Convênio Happy é o seu novo aliado se você ama cuidar do seu cabelo, corpo e bem estar. Nossa rede credenciada tem centenas de salões de beleza, milhares de profissionais disponíveis para cuidar da sua auto-estima.
                        <br />
                        <br />
                        Oferecemos descontos exclusivos, dicas de beleza entre outros atrativos.
                    </p>

                </div>

            </div>

            <div class="row">

                <div class="col-md-4">

                    <div class="half-bg"><div><h5>MISSÃO</h5></div></div>

                    <p>
                        Buscamos permanentemente a excelência credenciando as principais empresas do mercado da beleza. Acompanhamos de perto as empresas e profissionais para garantir a qualidade e a clareza para nosso cliente.
                    </p>

                </div>

                <div class="col-md-4">

                    <div class="half-bg"><div><h5>VISÃO</h5></div></div>

                    <p>
                        Acreditamos fielmente que <br />
                        a transparência solidifica marcas, <br />
                        e consequentemente <br />
                        o reconhecimento e a satisfação do cliente vem. Acompanhar a tecnologia, dessa forma que garantimos um sistema leve, dinâmico
                        <br />
                        e funcional.
                    </p>

                </div>

                <div class="col-md-4">

                    <div class="half-bg"><div><h5>VALORES</h5></div></div>

                    <p>
                        ● Comprometimento <br />
                        ● Transparencia <br />
                        ● Ética <br />
                        ● Responsabilidade <br />
                        ● Criatividade <br />
                        ● Inovação <br />
                        ● Novidades
                    </p>

                </div>

            </div>

        </div>

    </section>

    <!-- End of Content -->

<?php include_once( '../footer.php' ); ?>