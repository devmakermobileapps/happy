<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?php echo $dir_fixer; ?>assets/css/main.css" type="text/css" />
    <!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../header-modais.php' ); ?>

    <?php include_once( '../navbar.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/saloes-conveniados-bg.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        SALÕES
                    </h2>

                    <h1>CONVENIADOS</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="section">

        <div class="row conveniados">

            <div class="col-md-6 lojas">

                <h4>BUSCAR POR CEP/ENDEREÇO</h4>

                <div class="form-group row">

                    <div class="col-md-6">

                        <input type="text" class="form-control type-03" placeholder="00000-000" />

                    </div>

                    <div class="col-md-6">

                        <input type="text" class="form-control type-03" placeholder="ENDEREÇO" />

                    </div>

                </div>

                <div class="row scrollable">

                    <?php for($i=1; $i<=8; $i++){ ?>
                    <div class="col-md-6">

                        <div class="thumbnail" onclick="window.location.href = 'salao-conveniado.php';">

                            <img src="../assets/img/home/img%20(<?php echo $i; ?>).jpg" />

                            <div class="caption">

                                <h5>ESPAÇO ANDRY MOLINA</h5>

                                <ul class="stars">

                                    <li class="star"><i class="fa fa-star"></i></li>
                                    <li class="star"><i class="fa fa-star"></i></li>
                                    <li class="star active"><i class="fa fa-star"></i></li>
                                    <li class="star"><i class="fa fa-star"></i></li>
                                    <li class="star"><i class="fa fa-star"></i></li>

                                </ul>

                                <p>
                                    <i class="fa fa-map-marker"></i>
                                    Rua Vitório Marenda, 210 | Afonso Pena |
                                    São José dos Pinhais | PR
                                </p>

                                <div class="contact flex-row">

                                    <p><i class="fa fa-whatsapp"></i> 41.99650.1695</p>
                                    <p><i class="fa fa-phone"></i> 41.3534.2540</p>

                                </div>

                            </div>

                        </div>

                    </div>
                    <?php } ?>

                </div>

            </div>

            <div class="col-md-6">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.1237268657555!2d-49.284505935399665!3d-25.434126733786535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40ace042a9d%3A0x4f605bf9f2b75e58!2sCondom%C3%ADnio+Edif%C3%ADcio+Wall+Street+Center+-+Alameda+Dr.+Carlos+de+Carvalho%2C+771+-+Centro%2C+Curitiba+-+PR%2C+80430-100!5e0!3m2!1spt-BR!2sbr!4v1513706191138" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>

        </div>

    </section>

	<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>