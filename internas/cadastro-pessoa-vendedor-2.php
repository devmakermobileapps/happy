<!doctype html>

<html>

<?php include_once( '../init.php' ); ?>

<head>
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../assets/css/main.css" type="text/css" />
<!--    <meta http-equiv="refresh" content="1" />-->
</head>

<body>

    <!-- Modal -->

    <?php include_once( '../header-modais.php' ); ?>

    <?php include_once( '../navbar.php' ); ?>

    <div class="jumbotron" style="background-image:url('../assets/img/cadastrese.png');">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h2>
                        &nbsp;
                    </h2>

                    <h1>Cadastre-se</h1>

                </div>

            </div>

        </div>

    </div>

	<!-- Start of Content -->

    <section class="section pessoa-'ca-cadastre-se">

        <div class="container">

            <div class="row">

                <div class="col-md-6 form-lg">

                    <h5>DADOS CORPORATIVOS</h5>

                    <div class="form-group">

                        <label>Nome da empresa</label>
                        <input type="text" class="form-control type-03 lg" />

                    </div>

                    <div class="form-group">

                        <label>Razão social</label>
                        <input type="text" class="form-control type-03 lg" />

                    </div>

                    <div class="form-group">

                        <label>CNPJ</label>
                        <input type="text" class="form-control type-03 lg" placeholder="00.000.000/0001-00" />

                    </div>

                    <div class="form-group">

                        <label>Telefone</label>
                        <input type="text" class="form-control type-03 lg" placeholder="(00) 00000-0000" />

                    </div>

                    <div class="form-group">

                        <label>Site</label>
                        <input type="text" class="form-control type-03 lg" placeholder="www.seusite.com.br" />

                    </div>

                </div>

                <div class="col-md-6 form-lg">

                    <h5>DADOS PESSOAIS</h5>

                    <div class="form-group">

                        <label>Nome do responsável pela empresa</label>
                        <input type="text" class="form-control type-03 lg" placeholder="000.000.000-00" />

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Cargo</label>
                            <input type="text" class="form-control type-03 lg" />

                        </div>

                        <div class="col-md-6">

                            <label>Gênero</label>

                            <select class="select2 type-03 lg form-control">
                                <option>Masculino</option>
                                <option>Feminino</option>
                            </select>

                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">

                            <label>Telefone fixo</label>
                            <input type="text" class="form-control type-03 lg" placeholder="(00) 0000-0000" />

                        </div>

                        <div class="col-md-6">

                            <label>Whatsapp</label>
                            <input type="text" class="form-control type-03 lg" placeholder="(00) 00000-0000" />

                        </div>

                    </div>

                    <div class="form-group">

                        <label>Telefone</label>
                        <input type="text" class="form-control type-03 lg" placeholder="(00) 00000-0000" />

                    </div>

                    <div class="form-group">

                        <label>E-mail</label>
                        <input type="text" class="form-control type-03 lg" placeholder="email@seuemail.com.br" />

                    </div>

                    <div class="form-group">

                        <button onclick="window.location.href = 'cadastro-pessoa-vendedor-3.php';" class="btn default btn-block lg">CADASTRAR</button>

                    </div>

                </div>

            </div>

        </div>

    </section>

	<!-- End of Content -->

<?php include_once( '../footer.php' ); ?>