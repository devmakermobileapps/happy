<?php include_once( 'header.php' ); ?>

	<!-- Start of Content -->

    <section class="section default type-01">

        <div class="half-bg"><div><h3>VANTAGENS</h3></div></div>

        <div class="container">

            <div class="row steps home-steps">

                <div class="col-md-4" style="background-image:url(assets/img/1.png);">

                    <div class="row">

                        <div class="col-md-6 col-md-offset-3">

                            <p>
                                Faça seu cadastro, adquira a assinatura anual e ganhe na hora um produto da Keune e retire em um dos nossos pontos.
                            </p>

                        </div>

                    </div>

                </div>

                <div class="col-md-4" style="background-image:url(assets/img/2.png);">

                    <div class="row">

                        <div class="col-md-6 col-md-offset-3">

                            <p>
                                Você aproveita o desconto em centenas de serviços que os salões de beleza e clínicas de estéticas conveniaos oferecem.
                            </p>

                        </div>

                    </div>

                </div>

                <div class="col-md-4" style="background-image:url(assets/img/3.png);">

                    <div class="row">

                        <div class="col-md-6 col-md-offset-3">

                            <p>
                                São 15% de descontos nos principais Salões de Beleza e Clínica de Estética em Curitiba e Região Metropolitana.
                            </p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <section class="section bg" style="background-image:url(assets/img/home/bg-02.png"></section>

    <section class="section home">

        <div class="half-bg"><div><h3>TIME LINE</h3></div></div>

        <div class="row has-post">

            <div class="col-md-6 col-md-offset-3">

                <div class="posts">

                    <?php for($i=1; $i < 10; $i++){ ?>
                    <div class="post">
                        
                        <div class="header">

                            <div class="avatar" onclick="window.location.href = 'internas/perfil-profissional.php';">

                                <img src="assets/img/home/img%20(1).jpg" />

                            </div>

                            <div class="info" onclick="window.location.href = 'internas/perfil-profissional.php';">

                                <h5>Pérola Saint</h5>
                                <small>Espaço Andry Molina</small>

                            </div>

                            <h5 class="time"><i class="fa fa-clock-o"></i> 1h</h5>

                            <div class="clear"></div>

                        </div>
                        
                        <div class="body">
                            
                            <div class="image">

                                <img src="assets/img/home/img%20(1).jpg" class="img-responsive" />
                                
                            </div>

                            <div class="content">

                                <div class="actions">

                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-comments-o"></i>
                                    <p><small>72 curtidas</small></p>

                                </div>

                                <p class="comment">
                                    <strong>Pérola Saint</strong>
                                    Mechas lindas, realizadas hoje aqui no salão, gostou? <br />
                                    Entre em contato através do WhatsApp (41) 912345-1234.
                                </p>

                                <small class="view-all-comments">Ver todos os 12 comentários</small>

                                <p class="comment">
                                    <strong>Joana da Silva</strong>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                </p>

                                <p class="comment">
                                    <strong>José Mário Silva</strong>
                                    Lorem ipsum dolor sit amet, adipiscing elit, dots.
                                </p>

                                <input type="text" class="form-control type-02" placeholder="Adicione um comentário…" />

                            </div>
                            
                        </div>
                        
                    </div>
                    <?php } ?>
                    
                </div>

            </div>

        </div>

    </section>

    <section class="section">

        <div class="half-bg"><div><h3>SALÕES CONVENIADOS</h3></div></div>

        <div class="row conveniados">

            <div class="col-md-6 lojas">

                <h4>BUSCAR POR CEP/ENDEREÇO</h4>

                <div class="form-group row">

                    <div class="col-md-6">

                        <input type="text" class="form-control type-03" placeholder="00000-000" />

                    </div>

                    <div class="col-md-6">

                        <input type="text" class="form-control type-03" placeholder="ENDEREÇO" />

                    </div>

                </div>

                <div class="row scrollable">

                    <?php for($i=1; $i<=8; $i++){ ?>
                    <div class="col-md-6">

                        <div class="thumbnail">

                            <img src="assets/img/home/img%20(<?php echo $i; ?>).jpg" />

                            <div class="caption">

                                <h5>ESPAÇO ANDRY MOLINA</h5>

                                <ul class="stars">

                                    <li class="star"><i class="fa fa-star"></i></li>
                                    <li class="star"><i class="fa fa-star"></i></li>
                                    <li class="star active"><i class="fa fa-star"></i></li>
                                    <li class="star"><i class="fa fa-star"></i></li>
                                    <li class="star"><i class="fa fa-star"></i></li>

                                </ul>

                                <p>
                                    <i class="fa fa-map-marker"></i>
                                    Rua Vitório Marenda, 210 | Afonso Pena |
                                    São José dos Pinhais | PR
                                </p>

                                <div class="contact flex-row">

                                    <p><i class="fa fa-whatsapp"></i> 41.99650.1695</p>
                                    <p><i class="fa fa-phone"></i> 41.3534.2540</p>

                                </div>

                            </div>

                        </div>

                    </div>
                    <?php } ?>

                </div>

            </div>

            <div class="col-md-6">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.1237268657555!2d-49.284505935399665!3d-25.434126733786535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40ace042a9d%3A0x4f605bf9f2b75e58!2sCondom%C3%ADnio+Edif%C3%ADcio+Wall+Street+Center+-+Alameda+Dr.+Carlos+de+Carvalho%2C+771+-+Centro%2C+Curitiba+-+PR%2C+80430-100!5e0!3m2!1spt-BR!2sbr!4v1513706191138" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>

        </div>

    </section>

    <?php // include_once( 'nossa-loja.php' ); ?>

	<!-- End of Content -->

<?php include_once( 'footer.php' ); ?>