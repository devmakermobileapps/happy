<section class="nossa-loja">

    <div class="sidebar scrollable">

        <div class="form-group search">

            <div class="input-group">

                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span>

                <input type="search" class="form-control type-01" placeholder="Procurar" />

            </div>

        </div>

        <ul class="menu">

            <li href="#" class="menu-item">

                <i class="happy-icons">
                    <img src="<?php echo $dir_fixer; ?>assets/img/icons/nossa-loja/cabelos-white.svg" />
                </i>

                <h5>Cabelos</h5>

            </li>

            <li class="menu-item has-submenu active" data-submenu="perfumes">

                <i class="happy-icons">
                    <img src="<?php echo $dir_fixer; ?>assets/img/icons/nossa-loja/perfume-default.svg" />
                </i>

                <h5>Perfumes</h5>

            </li>

            <ul class="submenu" id="perfumes">

                <a href="#" class="submenu-item">Kits para presente</a>
                <a href="#" class="submenu-item">Feminino</a>
                <a href="#" class="submenu-item">Masculino</a>
                <a href="#" class="submenu-item">Unissex</a>

            </ul>

            <li class="menu-item has-submenu" data-submenu="maquiagem">

                <i class="happy-icons">
                    <img src="<?php echo $dir_fixer; ?>assets/img/icons/nossa-loja/batom-default.svg" />
                </i>

                <h5>Maquiagem</h5>

            </li>

            <ul class="submenu" id="maquiagem">

                <a href="#" class="submenu-item">Kits para presente</a>
                <a href="#" class="submenu-item">Feminino</a>
                <a href="#" class="submenu-item">Masculino</a>
                <a href="#" class="submenu-item">Unissex</a>

            </ul>

            <li class="menu-item has-submenu" data-submenu="cuidados-para-pele">

                <i class="happy-icons"></i>

                <h5>Cuidados para pele</h5>

            </li>

            <ul class="submenu" id="cuidados-para-pele">

                <a href="#" class="submenu-item">Kits para presente</a>
                <a href="#" class="submenu-item">Feminino</a>
                <a href="#" class="submenu-item">Masculino</a>
                <a href="#" class="submenu-item">Unissex</a>

            </ul>

            <li class="menu-item has-submenu" data-submenu="corpo-e-banho">

                <i class="happy-icons"></i>

                <h5>Corpo e banho</h5>

            </li>

            <ul class="submenu" id="corpo-e-banho">

                <a href="#" class="submenu-item">Kits para presente</a>
                <a href="#" class="submenu-item">Feminino</a>
                <a href="#" class="submenu-item">Masculino</a>
                <a href="#" class="submenu-item">Unissex</a>

            </ul>

        </ul>

        <div class="hor-line"></div>

        <div class="filters">

            <?php for($i=1; $i<7; $i++){ ?>
                <div class="form-group">

                    <input type="checkbox" id="filter-<?php echo $i; ?>" class="checkbox-default" />
                    <label for="filter-<?php echo $i; ?>">Lorem ipsum dolor sit.</label>

                </div>
            <?php } ?>

        </div>

    </div>

    <div class="content header">

        <div class="row">

            <div class="col-md-6">

                <p>CUIDE DO SEU VISUAL</p>

            </div>

            <div class="col-md-6">

                <div class="row">

                    <div class="col-md-3">

                        <p>Ver carrinho</p>

                    </div>

                    <div class="col-md-6">

                        <p>596 PRODUTOS ENCONTRADOS</p>

                    </div>

                    <div class="col-md-3">

                        <div class="dropdown">
                            <select class="select2 form-control type-02 white">
                                <option>Lançamentos</option>
                                <option>Menos preço</option>
                                <option>Maior preço</option>
                                <option>Promoção</option>
                            </select>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="content scrollable">

        <div class="row">

            <?php for($i=1; $i<10; $i++){ ?>
            <div class="col-md-3">

                <div class="thumbnail" data-toggle="modal" data-target="#produto_view-modal">

                    <span class="promotion">-30%</span>

                    <div class="image">

                        <img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="Produto" class="img-responsive" />

                    </div>

                    <div class="caption">

                        <p class="desc">
                            Kit Aussie Shampoo 400 ml +
                            Condicionardor 400 ml + 3 minute
                        </p>

                        <h5 class="sku">AHU6798</h5>

                        <h4 class="price">R$ 59,90</h4>

                        <div class="credit-card">
                            ou até 10x de R$5,90
                        </div>
                    </div>

                </div>

            </div>
            <?php } ?>

        </div>

        <div class="pagination">

            <ul>

                <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                <li><a href="#"><i class="fa fa-angle-left"></i></a></li>

                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">18</a></li>

                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>

            </ul>

        </div>

    </div>

</section>

<div class="modal type-01" id="produto_view-modal" role="dialog">

    <div class="modal-dialog modal-md">

        <div class="modal-content">

            <div class="modal-body">

                <div class="row">

                    <div class="col-md-6">

                        <div class="slick slider-for">
                            <?php for($i=1; $i<=8; $i++){ ?>
                                <div><img src="../assets/img/home/img%20(<?php echo $i; ?>).jpg" class="img-responsive" /></div>
                            <?php } ?>
                        </div>

                        <div class="slick slider-nav">
                            <?php for($i=1; $i<=8; $i++){ ?>
                                <div><img src="../assets/img/home/img%20(<?php echo $i; ?>).jpg" class="img-responsive" /></div>
                            <?php } ?>
                        </div>

                    </div>

                    <div class="col-md-6">

                        <h3 class="title">
                            Kit Aussie Shampoo 400ml +
                            Condicionador 400ml +
                            3 minute
                        </h3>

                        <span class="sku">AHU6798</span>

                        <p class="desc">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.
                        </p>

                        <label>Quantidade:</label>

                        <div class="form-group row">

                            <div class="col-md-3">

                                <input type="number" class=" form-control type-03" />

                            </div>

                        </div>

                        <div class="form-group">

                            <button class="btn default btn-block btn-lg">ADICIONAR AO CARINHO</button>

                        </div>

                        <a href="carrinho.php" class="ver-carrinho">Ver carrinho</a>

                    </div>

                </div>

                <h5 class="m-t-30 related">Clientes que viram este produto, também viram:</h5>

                <div class="container testimonial-group">

                    <div class="row text-center">

                        <?php for($i=1; $i<10; $i++){ ?>
                        <div class="col-xs-3">

                            <div class="thumbnail">

                                <span class="promotion">-30%</span>

                                <div class="image">

                                    <img src="<?php echo $dir_fixer; ?>assets/img/produto.jpg" alt="Produto" class="img-responsive" />

                                </div>

                                <div class="caption">

                                    <p class="desc">
                                        Kit Aussie Shampoo 400 ml +
                                        Condicionardor 400 ml + 3 minute
                                    </p>

                                    <h5 class="sku">AHU6798</h5>

                                    <h4 class="price">R$ 59,90</h4>

                                    <div class="credit-card">
                                        ou até 10x de R$5,90
                                    </div>
                                </div>

                            </div>

                        </div>
                        <?php } ?>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>